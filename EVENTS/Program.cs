using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVENTS
{
    //=======================================================================================================
    //This is Subscriber Class
    //=======================================================================================================
    class Program
    {
        static void Main(string[] args)
        {
            AddTwoNumbers a = new AddTwoNumbers();
            //Event gets binded with delegates
            a.ev_OddNumber += new AddTwoNumbers.dg_OddNumber(OddEventMessage);
            a.ev_EvenNumber += new AddTwoNumbers.dg_EvenNumber(EvenEventMessage);
            a.ev_EndLoop += new AddTwoNumbers.dg_EndLoop(EndLoopEventMessage);
            Console.WriteLine("Start to trigger events.. Key in any numbers, Enter Key to end..");

            a.Add();
        }
        
        //Delegates calls this method when ODD event raised.  
        static void OddEventMessage()
        {
            Console.WriteLine("\n\r********Event Triggered : This is Odd Number**********");
        }

        //Delegates calls this method when ODD event raised.  
        static void EvenEventMessage()
        {
            Console.WriteLine("\n\r********Event Triggered : This is Even Number**********");
        }

        //Delegates calls this method when End Loop event raised.  
        static void EndLoopEventMessage()
        {
            Console.WriteLine("\n\r\n\r\n\r\n\rEnd Loop Event Triggered...bye bye....!");
        }
    }

    //=======================================================================================================
    //This is Publisher Class
    //=======================================================================================================
    class AddTwoNumbers
    {
        public delegate void dg_OddNumber(); //Declared Delegate     
        public delegate void dg_EvenNumber(); //Declared Delegate
        public delegate void dg_EndLoop(); //Declared Delegate
        public event dg_OddNumber ev_OddNumber; //Declared Odd Events
        public event dg_EvenNumber ev_EvenNumber; //Declered Even Events
        public event dg_EndLoop ev_EndLoop; //Declared End Loop Events

        public void Add()
        {
            int result;
            result = 5 + 5;
 
            do
            {
                result  = Console.ReadKey().KeyChar;

                //Console.WriteLine(result);
                //Check if result is odd number then raise event

                if ((result % 2 != 0) && (ev_OddNumber != null) && (ev_OddNumber != null))
                {
                    ev_OddNumber(); //Raised Event
                }
                else
                {
                    ev_OddNumber(); //Raised Event
                }
            } while (result != 13);   //--'Enter' key

            ev_EndLoop();
        }
    }
}
